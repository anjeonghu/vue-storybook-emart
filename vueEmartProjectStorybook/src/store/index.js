/**
 * Created by front on 2018-08-13.
 */
import Vue from 'vue'
import Vuex from 'vuex'
import createLogger from 'vuex/dist/logger'
import templates from './modules/templates';

Vue.use(Vuex);

Vue.config.productionTip = false;
const debug = process.env.NODE_ENV !== 'production'

export default new Vuex.Store({
  modules: {
    templates
  },
  strict: debug,
  plugins: debug ? [createLogger()] : []
})
