/**
 * Created by front on 2018-08-13.
 */
import Vue from 'vue'
import axios from 'axios'
import { templateUtill } from '../../utill'
import { SET_TEMPLATES, UPDATE_TEMPLATES, INIT_TEMPLATES } from '../mutation-types'
import { baseUrl } from '../../Const'

// initial state
const state = {
  all: [],
  sizes: {
    containerCenterPos: {},
    containerWidth: 600,
    containerHeight: 600,
    tagsSize: {
      'name': {w: 195, h: 50},
      'scoreImgUrl': {w: 195, h: 50},
      'info': {w: 134, h: 56},
      'eventInfo': {w: 134, h: 56},
      'addInfo1': {w: 134, h: 56},
      'addInfo2': {w: 134, h: 56},
      'moreInfoUrl': {w: 134, h: 56},
      'originImgUrl': {w: 68, h: 68},
      'salesRankImgUrl': {w: 88, h: 88},
      'typeImgUrl': {w: 68, h: 68},
      'alphaVideoUrl': {w: 338, h: 219}
    }
  }
};

// getters
const getters = {
  isStoreEmpty: (state) => {
    return state.all.length > 0 ? false : true ;
  },
  getTemplateModelCopy: (state) => {
    return JSON.parse(JSON.stringify(state.all));
  },
  getAllTemplateModel: (state) => (scale) => {

    // jquery 나 lodash 를 이용하면 deep copy를 쉽게 할 수 있다.
    const sizes = JSON.parse(JSON.stringify(state.sizes));
    const templates = JSON.parse(JSON.stringify(state.all));
    const model = templateUtill.getTemplateModel(sizes, {data: templates, scale, unit: 'mm'});
    return model;
  },
  getTemplatesById: (state, getters, dispatch) => (id) => {

      let template = state.all.find(template => template.id === id);
      template = JSON.parse(JSON.stringify(template));
      return template;
  }
};

// actions
const actions = {
  getAllTemplates({ commit }) {
    commit('INIT_TEMPLATES');

    return axios.get(`${baseUrl}/Templates/`).
    then((result) => {
      commit('SET_TEMPLATES', result.data);
    }).catch(error => {
      console.log(error.message)
    });
  },
  getTemplate({ commit }, id) {
    commit('INIT_TEMPLATES');
    return axios.get(`${baseUrl}/Templates/${id}`).
    then((result) => {
      commit('SET_TEMPLATES', result.data);
    }).catch(error => {
      console.log(error.message)
    });
  },
  fetchTemplate({ commit }, payload) {
    return axios.put(`${baseUrl}/Templates/${payload.id}`, payload.req).then((result) => {
      console.log(result.data);
      commit('UPDATE_TEMPLATES', result.data);
    }).catch(function (error) {
      console.log(error)
    });
  }
};

// mutations
const mutations = {
  [INIT_TEMPLATES] (state){
    state.all = []
  },
  [SET_TEMPLATES] (state, data) {
      if(Array.isArray(data)){
        state.all = data;
      } else {
        state.all.push(data);
      }
  },
  [UPDATE_TEMPLATES] (state, { id, data }) {
    if (state.all.length > 0) {
      let template;
      template = state.all.find(template => template.id === id);
      template = data;
    } else {
      state.all.push(data);
    }
  }
};

export default {
  namespaced: true,
  state,
  getters,
  actions,
  mutations
}
