/**
 * Created by front on 2018-08-17.
 */
const templateUtil = templateUtil || {};

templateUtil.getCenter = (sizes) => {
  let x  = Math.floor(sizes.containerWidth / 2);
  let y  = Math.floor(sizes.containerHeight / 2);
  return {x, y};
};

templateUtil.xAxis =  (item, sizes, containerCenterPos, markerWidth) => {
  let x;
  if (item.align === 'center') {
    // 중심점을 기준으로 재정렬
    let targetCenterWidth = Math.floor(sizes.tagsSize[item.dataCol].w /2);
    x = containerCenterPos.x + (item.position.x * markerWidth) - targetCenterWidth;
  } else if (item.align === 'right') {
    // 오른쪽 중심점을 기준으로 재정렬
    x = containerCenterPos.x + (item.position.x * markerWidth) - sizes.tagsSize[item.dataCol].w;
  } else if (item.align === 'left') {
    // 왼쪽 중심점을 기준으로 재정렬
    x = containerCenterPos.x + (item.position.x * markerWidth);
  }
  return x;
};
templateUtil.yAxis =  (item, sizes, containerCenterPos, markerWidth) => {
  let y, targetCenterHeight;
  targetCenterHeight = Math.floor(sizes.tagsSize[item.dataCol].h /2);
  y = (containerCenterPos.y + (item.position.y * markerWidth)) - targetCenterHeight;

  return y;
};

templateUtil.getTemplateModel = (sizes, payload) => {
  let data, scale, unit;
  data = payload.data;
  scale = payload.scale;
  unit = payload.unit;

  unit = unit || 'mm';

  switch(unit) {
    case 'mm':
      unit = 0.264;
  }
  sizes.containerWidth *= scale;
  sizes.containerHeight *= scale;

  Object.keys(sizes.tagsSize).forEach((key) => {
    sizes.tagsSize[key].w *= scale;
    sizes.tagsSize[key].h *= scale;
  });

  const getSingleTemplateModel = (data) => {
    let template = data;
    template.markerWidth = Math.ceil(((template.markerWidth * 1000) * scale) / unit) ; // m -> mm -> px 단위로 변환
    template.markerHeight = Math.ceil(((template.markerHeight * 1000) * scale) / unit); // m -> mm -> px 단위로 변환
    template.itemsJsonText = JSON.parse(template.itemsJsonText);

    let containerCenterPos = sizes.containerCenterPos =  getCenter();
    let markerWidth = template.markerWidth;

    template.itemsJsonText.forEach((item) => {
      item.position.x =  templateUtil.xAxis(item, sizes, containerCenterPos, markerWidth);
      item.position.y = templateUtil.yAxis(item, sizes, containerCenterPos, markerWidth);
      item.position.z *= 1000;
    });

    return template;
  };

  const getAllTemplateModel = (data) => {
    let templateModels = data.map(template => {
      template.markerWidth = Math.ceil(((template.markerWidth * 1000) * scale) / unit) ; // m -> mm -> px 단위로 변환
      template.markerHeight = Math.ceil(((template.markerHeight * 1000) * scale) / unit); // m -> mm -> px 단위로 변환
      template.itemsJsonText = JSON.parse(template.itemsJsonText);

      let containerCenterPos = sizes.containerCenterPos =  templateUtil.getCenter(sizes);
      let markerWidth = template.markerWidth;

      template.itemsJsonText.forEach((item) => {
        item.position.x =  templateUtil.xAxis(item, sizes, containerCenterPos, markerWidth);
        item.position.y = templateUtil.yAxis(item, sizes, containerCenterPos, markerWidth);
        item.position.z *= 1000;
      });

      return template;
    });

    return templateModels;
  };

  const getTemplateModel = (data) => {
    if (Array.isArray(data)) {
      return getAllTemplateModel(data)
    } else {
      return getSingleTemplateModel(data)
    }
  };
  const templateModel = getTemplateModel(data);

  return {
    sizes,
    templateModel
  }
};
export default templateUtil;
// initial state
