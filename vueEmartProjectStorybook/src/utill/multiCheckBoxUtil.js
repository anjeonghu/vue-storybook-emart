// if (typeof jQuery === 'undefined' && typeof $ === 'undefined' ) {
//   throw new Error('multiCheckBox requires jQuery');
// };
;(function(factory) {
  if (typeof define === 'function' && define.amd) {
    // AMD. Register as an anonymous module depending on jQuery.
    define(['jquery'], factory);
  } else {
    // No AMD. Register plugin with global jQuery object.
    return factory;
  }
}(function($) {
  'use strict';
  var __bind = function(fn, me) {
    return function() {
      return fn.apply(me, arguments);
    };
  };

  return function(parent, child) {
    const MultiCheckBox = function(parent, child) {
      this.child = child;

      this.parent = parent;
      this.init();
    };

    MultiCheckBox.prototype = {
      init: function() {

        $(this.child).each(function() {
          $(this).addClass('single_checkbox');
        });
        this.events();
      },
      events: function() {
        $(this.parent).click(__bind(function(e) {
          e.stopPropagation();
          let target = e.target;
          this.checkToggleAll(target);
        }, this));

        $(this.child).click(__bind(function(e) {
          e.stopPropagation();
          this.checkToggleSingle();
        }, this))
      },
      checkToggleAll: function(target) {
        let checkBool = $(target).prop("checked");
        $(this.child).each(function() {
          $(this).prop("checked", checkBool);
        });
      },
      checkToggleSingle: function() {
        let checkBoxLength = $(this.child).length;
        let selectedLength = $(this.child).filter(':checked').length;
        if (checkBoxLength === selectedLength) {
          $(this.parent).removeClass('dash');
          $(this.parent).prop({
            'checked': true
          })
        } else if (selectedLength === 0) {
          $(this.parent).removeClass('dash');
          $(this.parent).prop({
            'checked': false
          })
        } else {
          $(this.parent).addClass('dash');
        }
      }
    }

    return  new MultiCheckBox(parent, child);

  }

}));
