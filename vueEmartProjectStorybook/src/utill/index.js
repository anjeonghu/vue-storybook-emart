/**
 * Created by front on 2018-08-20.
 */
import  templateUtill from './templateUtill'
import  multiFileUploadUtill from './multiFileUploadUtill'
import  multiCheckBoxUtil from './multiCheckBoxUtil'

export { templateUtill, multiFileUploadUtill, multiCheckBoxUtil }
