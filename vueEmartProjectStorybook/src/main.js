// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import App from './App'
import axios from 'axios'

Vue.config.productionTip = false
Vue.prototype.$http = axios.create({
  baseURL: 'http://192.168.0.16:8000/api/',
  // baseURL: '/api/',
  responseType: 'json'
});  // ajax request lib 전역 선언

// Vue.prototype.$http.defaults.headers.common['Authorization'] = 'Sec bGhpMjM1cjk4YWVyZ3Q5MDhlcmlpZWxkZmpiOWZ4OGdldFMkRVl0dXM5ZXI=';
// Vue.prototype.$http.defaults.headers.post['Content-Type'] = 'multipart/form-data';
  Vue.prototype.$http.defaults.headers.put['Content-Type'] = 'application/json';
  Vue.prototype.$http.defaults.headers.put['X-Requested-With'] = 'XMLHttpRequest';
// Vue.prototype.$http.defaults.headers.put['Content-Type'] = 'multipart/form-data';
/* eslint-disable no-new */
console.log('main!!!!');
new Vue({
  el: '#app',
  components: { App },
  template: '<App/>'
});
