/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'
import store from '../store'
import MultiFileUpload from '../components/MultiFileUploadUi.vue'

storiesOf('MultiFileUploadUi', module)
  .addDecorator(VueInfoAddon)
  .add('upload taget image', () => ({
    store,
    components: { MultiFileUpload },
    template: '<multi-file-upload></multi-file-upload>'
  }));

