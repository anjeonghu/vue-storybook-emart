/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import Pagination from '../components/Pagination/Pagination.vue'

var settings = {
    totalItemCount: 7,
    totalPageCount: 16,
    pageIndex: 1,
    pageSize: 50,
    hasNextPage: false,
    hasPreviousPage: false,
    isFirstPage: true,
    isLastPage: true
  };
storiesOf('Pagination', module)
  .addDecorator(VueInfoAddon)
    .add('Pagination', () => ({
      components: { Pagination },
      data () {
       return {
         settings
       };
      },
      template: '<Pagination  :limit=10 :settings="settings"></Pagination>'
    }))
