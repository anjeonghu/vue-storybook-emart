/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import ProductList from '../components/ProductList.vue'

storiesOf('ProductList', module)
  .addDecorator(VueInfoAddon)
  .add('product list', () => ({
    components: { ProductList },
    template: '<ProductList title="상품 목록"></ProductList>'
  }))

