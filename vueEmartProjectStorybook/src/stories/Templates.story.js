/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'
import store from '../store'
// import { withNotes } from '@storybook/addon-notes';

import Templates from '../components/Templates.vue'

storiesOf('Templates', module)
  .addDecorator(VueInfoAddon)
  .add('template list', () => (
    {
      store,
      components: { Templates },
      template: '<templates></templates>'
    }
  ));
