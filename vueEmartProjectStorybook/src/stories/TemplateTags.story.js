/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'
// import { withNotes } from '@storybook/addon-notes';

import TemplateTag from '../components/TemplateTags.vue'

storiesOf('TemplateTag', module)
  .addDecorator(VueInfoAddon)
  .add('align right', () => (
    //  withNotes('type text')(
    {
      components: { TemplateTag },
      data: function (){
        return {
          right:[
            {
              type: 'text',
              dataCol: 'name',
              name: '상품명',
              bgColor: 'blue',
              align: 'right'
            }
          ]
        }
      },
      template: '<template-tag :item="right[0]"></template-tag>'
    }
    //   )
  ))
  .add('align left', () => (
    //  withNotes('type text')(
    {
      components: { TemplateTag },
      data: function (){
        return {
          left: [
            {
              type: 'text',
              dataCol: 'name',
              name: '활용 정보',
              bgColor: 'green',
              align: 'left'
            }
          ]
        }
      },
      template: '<template-tag :item="left[0]"></template-tag>'
    }
    //   )
  ))
  .add('align center', () => (
    //  withNotes('type text')(
    {
      components: { TemplateTag },
      data: function (){
        return {
          center: [
            {
              type: 'text',
              dataCol: 'name',
              name: '부가 정보',
              bgColor: 'gray',
              align: 'center'
            }
          ]
        }
      },
      template: '<template-tag :item="center[0]"></template-tag>'
    }
    //   )
  ));
