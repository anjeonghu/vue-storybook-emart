/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import Trackable from '../components/Trackable.vue'
import Modal from '../components/Modal/Modal.vue'

storiesOf('Modal', module)
  .addDecorator(VueInfoAddon)
  .add('Trackables Modal', () => ({
    components: { Modal, Trackable },
    template: '<Modal :show="true" title="등록된 타깃 이미지"><Trackable></Trackable></Modal>'
  }))
