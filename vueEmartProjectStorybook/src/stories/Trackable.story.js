/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import Trackable from '../components/Trackable.vue'

storiesOf('Trackable', module)
  .addDecorator(VueInfoAddon)
    .add('Trackable', () => ({
      components: { Trackable },
      template: '<Trackable ></Trackable>'
    }))
