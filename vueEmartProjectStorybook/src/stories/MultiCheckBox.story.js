/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import MultiCheckBox from '../components/MultiCheckBox'

storiesOf('MultiCheckBox', module)
  .addDecorator(VueInfoAddon)
    .add('multi check box', () => ({
      components: { MultiCheckBox},
      template: '<multi-check-box  title="multi check box"></multi-check-box>'
    }))
