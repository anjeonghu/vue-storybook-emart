/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'

import SingleCheckBox from '../components/SingleCheckBox.vue'

storiesOf('SingleCheckBox', module)
  .addDecorator(VueInfoAddon)
    .add('single check box', () => ({
      components: { SingleCheckBox },
      template: '<single-check-box  id="parent" cname="parent"></single-check-box>'
    }));
