/**
 * Created by front on 2018-07-11.
 */
import { storiesOf } from '@storybook/vue'
import VueInfoAddon from 'storybook-addon-vue-info'
import store from '../store'
import Draggable from '../components/DraggableUi.vue'

storiesOf('DraggableUi', module)
  .addDecorator(VueInfoAddon)
  .add('edit template', () => ({
    store,
    components: { Draggable },
    template: '<Draggable></Draggable>'
  }));

