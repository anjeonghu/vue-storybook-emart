/**
 * Created by front on 2018-07-11.
 */
import Vue from 'vue';

import { storiesOf } from '@storybook/vue';
import App from '../App.vue'

storiesOf('App', module)
  .add('common component', () => ({
    components: { App },
    template: '<app></app>'
  }))

