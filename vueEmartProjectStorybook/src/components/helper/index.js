/**
 * Created by front on 2018-01-24.
 */
export default {
  install: (Vue) => {
    Vue.prototype.$helpers = {
      dateFormat (date, format = 'yyyyMMdd') {
        const dt = new Date(date);
        let year = dt.getFullYear();
        let month = dt.getMonth() + 1;
        let day = dt.getDate();
        return format.replace(/(yyyy|MM|dd)/gi, ($1) => {
          switch ($1) {
            case 'yyyy':
              return year;
            case 'MM':
              return (2 - month.toString().length) > 0 ? '0' + month : month;
            case 'dd':
              return (2 - day.toString().length) > 0 ? '0' + day : day;
          }
        });
      },
      disableVideoOnMobile () {
        let isMobile = true;
        const bgv = $('video');
        if ($(window).width() > 700) {
          isMobile = false;
          $('source', bgv).each(function () {
            let el = $(this);
            el.attr('src', el.data('src'));
          });
        }
        return isMobile;
      },
    };
  }
};
