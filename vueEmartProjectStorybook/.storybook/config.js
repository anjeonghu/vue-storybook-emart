/**
 * Created by front on 2018-07-11.
 */
import { configure } from '@storybook/vue';

import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import generalHelpers from '../src/components/helper'
import store from '../src/store'
import 'expose-loader?$!expose-loader?jQuery!jquery'

Vue.use(Vuex)
Vue.use(generalHelpers);

Vue.prototype.$http = axios.create({
  baseURL: '/api/',
  // baseURL: 'http://192.168.0.16:8000/api/',
  responseType: 'json'
});  // ajax request lib 전역 선언

function loadStories() {
  // You can require as many stories as you need.
  const load = req => req.keys().map(req)

  load(require.context('../src/stories', true, /\.story\.js$/))
}

configure(loadStories, module);
